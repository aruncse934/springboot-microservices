package com.ar.webclient.controller;

import com.ar.webclient.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

    @Autowired
    WebClient webClient;


    //GetMapping
    public Flux<Employee> findAll() {
        return webClient.get().uri("/employees").retrieve().bodyToFlux(Employee.class);
    }

    public Mono<Employee> findById(Integer id) {
        return webClient.get().uri("/employees/" + id).retrieve()
                /*.onStatus(httpStatus -> HttpStatus.NOT_FOUND.equals(httpStatus),
                        clientResponse -> Mono.empty())*/
                .bodyToMono(Employee.class);
    }

    //PostMapping

    public Mono<Employee> create(Employee empl) {
        return webClient.post().uri("/employees").body(Mono.just(empl), Employee.class).retrieve().bodyToMono(Employee.class);
    }

    //PUTMapping
    public Mono<Employee> update(Employee e) {
        return webClient.put().uri("/employees/" + e.getId()).body(Mono.just(e), Employee.class).retrieve().bodyToMono(Employee.class);
    }

    //DeleteMapping
    public Mono<Void> delete(Integer id) {
        return webClient.delete().uri("/employees/" +id).retrieve().bodyToMono(Void.class);
    }
}
