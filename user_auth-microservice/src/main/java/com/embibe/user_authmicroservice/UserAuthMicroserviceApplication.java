package com.embibe.user_authmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserAuthMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserAuthMicroserviceApplication.class, args);

		System.out.println("Hello, Welcome to Spring Boot Application");
	}

}
