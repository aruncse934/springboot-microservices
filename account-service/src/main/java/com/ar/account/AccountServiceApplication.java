package com.ar.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@EnableReactiveMongoRepositories
public class AccountServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountServiceApplication.class, args);

		System.out.println("Hello,Welcome to Spring mongo Reactive Applications");
	}

}

/*{
    "userId": 1022,
    "orgId" : 2,
	"expiry": 1598599763002,
	"roles" : [1, 3, 2],
      "deviceId": 27849273
}
*/

/*"first_name" : str,
  "last_name" : str,
  "email" : str,
  "password" : str(hash),
  "mobile" : numeric,
 # "otp_verified" : bool,
 # "email_verified" : bool,
  "userType" : numeric,
  "isChildOf" : numeric, 			// Parent’s userID
  "isStudentOf" : numeric,			// Institute/Teacher’s userId
  "deviceId" : numeric,
  "additional_information" : json // Any other information that may be required like gender/dob etc.

*/
