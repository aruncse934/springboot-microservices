package com.ar.account.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@Document(collection = "account_service")
public class Account {
    private String id;
    private String name;
    private String email;
    private String password;

}
