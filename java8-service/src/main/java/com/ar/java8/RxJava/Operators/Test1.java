package com.ar.java8.RxJava.Operators;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;



public class Test1 {
    public static void main(String[] args) {
        Observable.just(1,2,3).map(new Func1<Integer,Integer>(){

            @Override
            public Integer call(Integer integer) {
                return integer*integer;
            }
        }).subscribe(new Action1<Integer>(){
                    @Override
                    public void call(Integer integer) {
                        System.out.println("square "+integer);
                    }
                });

    }

}
