package com.ar.java8.RxJava;
import io.reactivex.Flowable;

public class Test {
        public static void main(String[] args) {
            Flowable.just("Hello! Welcome to Reactive Java").subscribe(System.out::println);
        }
}
