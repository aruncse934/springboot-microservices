package com.ar.java8.RxJava.Observables;

import io.reactivex.Maybe;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableMaybeObserver;
import io.reactivex.schedulers.Schedulers;

import java.util.concurrent.TimeUnit;

public class ObservableTester {
    public static void main(String[] args) throws InterruptedException {
        //Create an Observer
        Disposable disposable = Maybe.just("Hello Welcome").delay(2, TimeUnit.SECONDS, Schedulers.io())
                .subscribeWith(new DisposableMaybeObserver<String>(){

                    @Override
                    public void onSuccess(String s) {
                        System.out.println(s);

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();

                    }

                    @Override
                    public void onComplete() {
                        System.out.println("Done!");
                    }
                });

        Thread.sleep(30000);
        //start observing
        disposable.dispose();
    }

}
