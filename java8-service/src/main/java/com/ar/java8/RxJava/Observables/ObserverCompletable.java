package com.ar.java8.RxJava.Observables;

import io.reactivex.Completable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;
import reactor.core.scheduler.Scheduler;

import java.util.concurrent.TimeUnit;

public class ObserverCompletable {
    public static void main(String[] args) throws InterruptedException {
        //Create an observer
        Disposable disposable = Completable.complete()
                .delay(2, TimeUnit.SECONDS, Schedulers.io())
                .subscribeWith(new DisposableCompletableObserver() {

                    @Override
                    public void onComplete() {
                        System.out.println("Done!");
                    }
                    @Override
                    public void onStart() {
                        System.out.println("Started!");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();

                    }
                });
        Thread.sleep(3000);
        //start observing
        disposable.dispose();

    }
}
