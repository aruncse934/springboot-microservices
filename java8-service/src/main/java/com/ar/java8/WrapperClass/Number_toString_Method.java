package com.ar.java8.WrapperClass;

public class Number_toString_Method {
    public static void main(String[] args) {
        // toString() method
        Integer x =12;
        System.out.println(x.toString());

        // toString(int i) method
        System.out.println(Integer.toString(12));

        System.out.println(Integer.toBinaryString(152));
        System.out.println(Integer.toHexString(152));
        System.out.println(Integer.toOctalString(152));

    }
}
