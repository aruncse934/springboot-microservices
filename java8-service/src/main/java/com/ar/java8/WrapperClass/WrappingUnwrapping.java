package com.ar.java8.WrapperClass;

public class WrappingUnwrapping {
    public static void main(String[] args) {

        //byte data type
        byte a =1;

        //wrapping around byte object
        Byte aByte = new Byte(a);

       // int data type
        int b = 10;

        //wrapping around Integer object
        Integer  integer = new Integer(b);

        //float data type
        float c = 18.6f;

        //wrapping around float object
        Float aFloat = new Float(c);

        //double data type
        double d = 250.5;

        //wrapping around Double object
        Double aDouble = new Double(d);

        //char data type
        char e = 'a';

        //wrapping around Character object
        Character character = e;

        //  printing the values from objects
        System.out.println("Values of Wrapper objects (printing as objects)");
        System.out.println("Byte object byteobj:  " + aByte);
        System.out.println("Integer object intobj:  " + integer);
        System.out.println("Float object floatobj:  " + aFloat);
        System.out.println("Double object doubleobj:  " + aDouble);
        System.out.println("Character object charobj:  " + character);

        //objects to data types (retrieving data types from objects)
        //unwrapping objects to primitive data types
        byte bv = aByte;
        int iv  = integer;
        float fv = aFloat;
        double dv = aDouble;
        char cv = character;


        // printing the values from data types
        System.out.println("Unwrapped values (printing as data types)");
        System.out.println("byte value, bv: " + bv);
        System.out.println("int value, iv: " + iv);
        System.out.println("float value, fv: " + fv);
        System.out.println("double value, dv: " + dv);
        System.out.println("char value, cv: " + cv);
    }
}
