package com.ar.java8.WrapperClass;

public class PrimitiveWrapperClassImmutable {
    private static void modify(Integer i){
        i = i+1;
    }
    public static void main(String[] args) {
        Integer i = new Integer(12);
        System.out.println(i);
        modify(i);
        System.out.println(i);
    }

}
