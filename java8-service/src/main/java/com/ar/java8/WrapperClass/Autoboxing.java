package com.ar.java8.WrapperClass;

import java.util.ArrayList;
import java.util.List;

public class Autoboxing {
    public static void main(String[] args) {
        char ch = 'a';
        //Autoboxing primitive to Character object conversion
        Character character = ch;
        List<Integer> list = new ArrayList<>();
        //Autoboxing because ArrayList stores only objects
        list.add(25);
        //printing the values from object
        System.out.println(list.get(0));
    }
}
