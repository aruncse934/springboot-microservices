package com.ar.java8.WrapperClass;

import java.util.ArrayList;
import java.util.List;

public class Unboxing {
    public static void main(String[] args) {
        Character ch = 'a';
        //unboxing -Character object to primitive conversion
        char c = ch;

        List<Integer> list = new ArrayList<Integer>();
        list.add(24);

        //unboxing because get method returns an Integer object
        int num = list.get(0);

        //printing the values from primitive data types
        System.out.println(num);
    }
}